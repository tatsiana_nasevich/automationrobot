*** Settings ***
Documentation  Base suite
Resource       ../Data/InputData.robot
Resource       ../Resources/ScriptApp.robot
Resource       ../Resources/Common.robot
Test Setup     Common.Begin Web Test
Test Teardown  Common.End Web Test

# run scripts with screnshots: robot  --listener CustomLibraries/ScreenshotListener.py  -d Results tests/Scripts.robot
# run scripts: robot -d Results tests/Scripts.robot
# run scripts with different browser: robot --listener CustomLibraries/ScreenshotListener.py -d Results -v BROWSER:chrome tests/Scripts.robot

*** Variables ***

*** Test Cases ***
Should Be Able To Login With Valid Credentials
    [Documentation]  Test the login
    [Tags]  1  Smoke  Login
    ScriptApp.Open "Login" modal
    ScriptApp.Login with valid credentials  ${REGISTERED_USER}

Should Be Able To Add Product To The Cart
    [Documentation]  Test adding products to cart
    [Tags]  2  Smoke  Cart
    ScriptApp.Open "Monitors" category
    ScriptApp.Select the product with the highest price on the page
    ScriptApp.Add to the cart
    ScriptApp.Check the cart



