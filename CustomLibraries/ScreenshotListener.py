from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
from robot.libraries.Screenshot import Screenshot


class ScreenshotListener:
    ROBOT_LISTENER_API_VERSION = 2

    @staticmethod
    def end_keyword(name, attrs):
        timestamp = BuiltIn().get_time(format='%Y%m%d%H%M%S').replace(':', '_')
        screenshot_path = f".//Screenshots//{name}_{timestamp}.png".replace(' ', '_').replace('"', '')

        if isinstance(attrs, dict):
            tags = attrs.get('tags', [])
            if 'screenshot' in tags:
                Screenshot().take_screenshot_without_embedding(name=screenshot_path)
                logger.info(f"Screenshot captured: {screenshot_path}")
