*** Settings ***
Documentation  Categories Page Obgect Keywords and locators
Library        SeleniumLibrary

*** Variables ***
${CATEGORIES_MONITORS_LINK}  xpath=//a[contains(@onclick,'monitor')]

*** Keywords ***
Go to "Monitors" category
    Click Link    ${CATEGORIES_MONITORS_LINK}