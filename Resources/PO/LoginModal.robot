*** Settings ***
Documentation  LoginModal Page Obgect Keywords and locators
Library        SeleniumLibrary

*** Variables ***
${LOGINMODAL_LABLE}           xpath=//h5[@id="logInModalLabel"]
${LOGINMODAL_USERNAME_INPUT}  xpath=//input[@id="loginusername"]
${LOGINMODAL_PASSWORD_INPUT}  xpath=//input[@id="loginpassword"]
${LOGINMODAL_LOGIN_BUTTON}    xpath=//button[@onclick="logIn()"]

*** Keywords ***
Verify Page Loaded
    Wait Until Page Contains Element    ${LOGINMODAL_LABLE}

Verify Page Content
    Page Should Contain Element    ${LOGINMODAL_USERNAME_INPUT}
    Page Should Contain Element    ${LOGINMODAL_PASSWORD_INPUT}
    Sleep  0.5s

Enter Credential
    [Arguments]   ${Credentials}
    Wait Until Element Is Visible    ${LOGINMODAL_USERNAME_INPUT}
    Input Text    ${LOGINMODAL_USERNAME_INPUT}  ${Credentials.Username}
    Wait Until Element Is Visible    ${LOGINMODAL_PASSWORD_INPUT}
    Input Text    ${LOGINMODAL_PASSWORD_INPUT}  ${Credentials.Password}

Cliclk "Log in" button
    Click Button    ${LOGINMODAL_LOGIN_BUTTON}

