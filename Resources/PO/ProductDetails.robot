*** Settings ***
Documentation  ProductDetails Page Obgect Keywords and locators
Library        SeleniumLibrary
Resource       ./ProductList.robot


*** Variables ***
${PRODUCTDETAILS_ADD_TO_CART_LINK}   link=Add to cart
${PRODUCTDETAILS_PRODUCT_NAME}       xpath=//h2[@class="name"]
${PRODUCTDETAILS_PRODUCT_PRICE}      xpath=//h3[@class='price-container']

*** Keywords ***
Verify Appropriate Page Loaded
    Wait Until Page Contains Element  ${PRODUCTDETAILS_PRODUCT_NAME}
    ${current_name}  Get Text    ${PRODUCTDETAILS_PRODUCT_NAME}
    ${current_price_full}  Get Text  ${PRODUCTDETAILS_PRODUCT_PRICE}
    ${current_price}  Set Variable  ${current_price_full.split()[0]}
    Should Be Equal As Strings    ${current_name}  ${PRODUCT_NAME_VALUE}
    Should Be Equal As Strings    ${current_price}  ${MAX_PRICE_VALUE}

Click "Add to cart" link
    Click Link    ${PRODUCTDETAILS_ADD_TO_CART_LINK}

Accept Alert
    ${alert_message}    Handle Alert    accept
