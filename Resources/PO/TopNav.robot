*** Settings ***
Documentation  TopNav Page Obgect Keywords and locators
Library        SeleniumLibrary

*** Variables ***
${TOPNAV_LOGIN_LINK}   xpath=//a[@id="login2"]
${TOPNAV_LOGOUT_LINK}  xpath=//a[@id="logout2"]
${TOPNAV_WELCOM_LINK}  xpath=//a[@id="nameofuser"]
${TOPNAV_CART_LINK}    xpath=//a[@ID="cartur"]


*** Keywords ***
Click "Login" link
    Click Link    ${TOPNAV_LOGIN_LINK}
    
Verify Elements After login
    Wait Until Page Contains Element    ${TOPNAV_LOGOUT_LINK}
    Verify Welkom link text

Verify Welkom link text
    Wait Until Element Is Visible    ${TOPNAV_WELCOM_LINK}
    ${link_text}  Get Text  ${TOPNAV_WELCOM_LINK}
    Log  ${link_text}
    Should Be Equal As Strings    ${link_text}  Welcome ${REGISTERED_USER.Username}

Click "Cart" link
    Click Link  ${TOPNAV_CART_LINK}

        