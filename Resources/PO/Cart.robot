*** Settings ***
Documentation  Cart Page Obgect Keywords and locators
Library        SeleniumLibrary
Resource       ./ProductList.robot

*** Variables ***
${CART_TITLE}  Products
${CART_PRODUCT_NAME}   xpath=//*[@id="tbodyid"]/tr/td[2]
${CART_PRODUCT_PRICE}  xpath=//*[@id="tbodyid"]/tr/td[3]

*** Keywords ***
Verify Page Loaded
    Wait Until Page Contains    ${CART_TITLE}

Verify Produdt Is Added
    Wait Until Page Contains Element  ${CART_PRODUCT_NAME}
    ${current_name}  Get Text  ${CART_PRODUCT_NAME}
    ${current_price}  Get Text  ${CART_PRODUCT_PRICE}
    Should Be Equal As Strings    ${current_name}  ${PRODUCT_NAME_VALUE}
    Should Be Equal As Strings    ${current_price}    ${MAX_PRICE_VALUE[1:]}


