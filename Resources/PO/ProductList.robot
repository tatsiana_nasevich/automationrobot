*** Settings ***
Documentation  ProductList Page Obgect Keywords and locators
Library        SeleniumLibrary
Library    Collections

*** Variables ***
${PRODUCTLIST_PRICE_LOCATOR}   xpath=//div[@class="card-block"]/h5
${PRODUCTLIST_PRODUCT_LINK}    xpath=//div[@class="card-block"]/h4/a
${PRODUCT_NAME_VALUE}                ${EMPTY}
${MAX_PRICE_VALUE}                   ${EMPTY}

*** Keywords ***
Verfify Page Loaded
    Sleep    2s
    Wait Until Element Is Visible   ${PRODUCTLIST_PRICE_LOCATOR}

Find Product with highest price on the page
    @{price_elements}    Get WebElements    ${PRODUCTLIST_PRICE_LOCATOR}
    ${prices}    Create List

    FOR    ${element}    IN    @{price_elements}
        ${price}    Get Text    ${element}
        Append To List    ${prices}    ${price}
    END

    ${maximum_price}    Evaluate    max(@{prices})
    Set Global Variable    ${MAX_PRICE_VALUE}    ${maximum_price}
    ${index}    Get Index From List   ${prices}    ${maximum_price}
    @{product_links}  Get WebElements    ${PRODUCTLIST_PRODUCT_LINK}
    ${PRODUCT_WITH_HIGHEST_PRICE}  Get From List  ${product_links}  ${index}
    RETURN  ${PRODUCT_WITH_HIGHEST_PRICE}

Open Product Details Page
    [Arguments]  ${product_link}
    ${product}  Get Text    ${product_link}
    Set Global Variable  ${PRODUCT_NAME_VALUE}  ${product}
    Click Link    ${product_link}
