*** Settings ***
Documentation  Application keywords
Resource       ./PO/TopNav.robot
Resource       ./PO/LoginModal.robot
Resource       ./PO/Categories.robot
Resource       ./PO/ProductList.robot
Resource       ./PO/ProductDetails.robot
Resource       ./PO/Cart.robot
Library         DateTime


*** Keywords ***
Open "Login" modal
    [Tags]    screenshot
    TopNav.Click "Login" link
    LoginModal.Verify Page Loaded
    LoginModal.Verify Page Content


Login with valid credentials
    [Tags]    screenshot
    [Arguments]  ${Credentials}
    LoginModal.Enter Credential  ${Credentials}
    LoginModal.Cliclk "Log in" button
    TopNav.Verify Elements After login

Open "Monitors" category
    Categories.Go to "Monitors" category
    ProductList.Verfify Page Loaded

Select the product with the highest price on the page
    [Tags]    screenshot
    ${highest_price_product_link}=  ProductList.Find Product with highest price on the page
    ProductList.Open Product Details Page  ${highest_price_product_link}
    ProductDetails.Verify Appropriate Page Loaded

Add to the cart
    ProductDetails.Click "Add to cart" link
    ProductDetails.Accept Alert

Check the cart
    [Tags]    screenshot
    TopNav.Click "Cart" link
    Cart.Verify Page Loaded
    Cart.Verify Produdt Is Added




   