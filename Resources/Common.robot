*** Settings ***
Documentation  Test Setup and Teardown
Library        SeleniumLibrary
Resource       ../Data/InputData.robot

*** Variables ***
${LOGO}  xpath=//*[@id="nava"]

*** Keywords ***
Begin Web Test
    Open Browser  ${URL}  ${BROWSER}
    Wait Until Page Contains Element    //body    timeout=30s
    Maximize Browser Window
    Wait Until Page Contains Element    ${LOGO}

End Web Test
    Close All Browsers    